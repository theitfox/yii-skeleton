<?php
$config = [
    'timeZone' => 'Australia/Melbourne', // Insert the timezone you want to use across your application
    'components' => [
        'request' => [
            'enableCookieValidation' => true,
            'cookieValidationKey' => 'OFw-c6MFZhreoh-MZ5g2vQgOGFjgSqHd', // Insert your secret key
        ],
    ],
];

/* Enable this section to enable gii access when the app is not in debug mode
if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ["*"]
    ];
}
*/

return $config;


