<?php

// You should know what to do
// Default, this is the config for local development environment with vagrant
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=vagrant',
    'username' => 'vagrant',
    'password' => 'vagrant',
    'charset' => 'utf8',
];


