<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\User;

class ProfileController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
    	$user = Yii::$app->user->identity;
        return $this->render('index', [ 'model' => $user ]);
    }

    public function actionUpdate()
    {
    	$model = Yii::$app->user->identity;
        $model->scenario = User::SCENARIO_UPDATE_PROFILE;

        if ($model->load(Yii::$app->request->post())) {

            if(!empty($model->_password)) {
                $model->setPassword($model->_password);
            }
            if($model->save()) {
            	return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
}
