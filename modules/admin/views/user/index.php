<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'username',
            [
                'attribute' => 'role',
                'content' => function($item, $key, $index, $column) {
                    return $item->getRole();
                }
            ],
            [
                'attribute' => 'status',
                'content' => function($item, $key, $index, $column) {
                    return $item->status == User::STATUS_ACTIVE ? "ACTIVE" : "INACTIVE";
                }
            ],
            'created_at:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {time-sheet}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('View', $url, [
                            'class' => 'btn btn-primary btn-sm'
                        ]);
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a('Update', $url, [
                            'class' => 'btn btn-info btn-sm'
                        ]);
                    },
                    'time-sheet' => function ($url, $model, $key) {
                        return Html::a('Timesheet', $url, [
                            'class' => 'btn btn-success btn-sm',
                        ]);
                    },
                ]
            ],
        ],
    ]); ?>

</div>