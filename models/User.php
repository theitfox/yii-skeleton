<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $name
 * @property string $password_hash
 * @property string $email
 * @property integer $status
 * @property string $auth_key
 * @property string $password_reset_token
 * @property string $access_token
 * @property string $created_at
 * @property string $updated_at
 */
class User extends ActiveRecord implements IdentityInterface
{

    const SCENARIO_CREATE = 'create';
    const SCENARIO_CONSOLE_CREATE = 'console_create';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_UPDATE_PROFILE = 'update_profile';

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $_password;
    public $_role;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['username', 'name', 'email', '_password', '_role', 'status'],
            self::SCENARIO_CONSOLE_CREATE => ['username', 'name', 'email', 'status'],
            self::SCENARIO_UPDATE => ['username', 'name', 'email', '_password', '_role', 'status'],
            self::SCENARIO_UPDATE_PROFILE => ['name', 'email', '_password'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'name', 'email', '_password', '_role'], 'required', 'on' => self::SCENARIO_CREATE],
            [['username', 'name', 'email'], 'required', 'on' => self::SCENARIO_CONSOLE_CREATE],
            [['username', 'name', 'email', '_role'], 'required', 'on' => self::SCENARIO_UPDATE],
            [['name', 'email'], 'required', 'on' => self::SCENARIO_UPDATE_PROFILE],
            [['status'], 'integer'],
            [['_password', 'auth_key', 'access_token'], 'safe'],
            [['username', 'name', 'password_hash', 'email', 'password_reset_token', 'access_token'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'name' => 'Display Name',
            '_password' => 'Password',
            '_role' => 'Role',
            'password_hash' => 'Password Hash',
            'email' => 'Email',
            'status' => 'Status',
            'auth_key' => 'Auth Key',
            'password_reset_token' => 'Password Reset Token',
            'access_token' => 'Access Token',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getId()
    {
        return $this->id;
    }

    public function getRole(){
        foreach (Yii::$app->authManager->getRolesByUser($this->id) as $roleName => $role) {
            return $roleName;
        }
        return NULL;
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
    * Validates password
    *
    * @param  string  $password password to validate
    * @return boolean if password provided is valid for current user
    */
    public function validatePassword($password)
    {
        if ($password !== NULL) {
            return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
        }
        return false;
    }

    public function setPassword($password) {
        $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function generateAccessToken()
    {
        $this->access_token = Yii::$app->security->generateRandomString();
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function save($runValidation = true, $attributeNames = NULL) {
        $this->updated_at = date('Y-m-d H:i:s');
        return parent::save($runValidation, $attributeNames);
   }
}
